FROM python:latest

RUN apt-get update && \
    apt-get install -y autoconf automake libtool curl make g++ unzip build-essential && \
    rm -rf /var/lib/apt/lists/*

# install protobuf
RUN wget https://github.com/google/protobuf/releases/download/v2.6.1/protobuf-2.6.1.tar.gz && \
    tar xzf protobuf-2.6.1.tar.gz && \
    cd protobuf-2.6.1 && \
    ./configure && \
    make && \
    make check && \
    make install  && \
    ldconfig

RUN protoc --version

ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8

ADD protobuf_builder /protobuf_builder

RUN pip3 install protobuf

RUN echo $(protoc --version)

CMD [ "python3", "./build_protocol.py" ]
