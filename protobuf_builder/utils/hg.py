from __future__ import print_function

import os
import shlex
import subprocess

from utils import logged
from utils.system import cd, add_env_var
from utils.log import log

NOTHING_TO_COMMIT_MSG = "nothing to commit"


class RepoError(Exception):
    pass


hg_path = None


def find_command(command):
    add_env_var('/opt/local/bin')
    return command


def _repo_cmd(path, cmd):
    with cd(path):
        cmd = find_command('hg') + " " + cmd
        cmd = shlex.split(cmd)
        log.debug("command: '{}'".format(cmd))

        process = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = process.communicate()

        log.debug(
            "cmd: '{}'. out: '{}'. error: '{}'".format(cmd, output, error))

        if "unknown command" in error:
            msg = "Unknown command: '{cmd}'. output:'{out}' error: '{error}'".format(
                cmd=cmd, out=output, error=error)
            raise RepoError(msg)

        if "abort" in error:
            is_test_repo = 'repository default not found!' in error \
                           or 'default repository not configured' in error

            if not is_test_repo:
                msg = """Command failed: '{cmd}' for repo '{path}'.
output: '{output}'

error: '{error}'""".format(
                    cmd=cmd, path=path, output=output, error=error)
                raise RepoError(msg)

        return output


def repo_init(path):
    return _repo_cmd(path, "init")


@logged(timed=True, print_args=True)
def repo_pull(path, update=True):
    assert os.path.exists(path), "repo '{}' not found".format(path)
    status = _repo_cmd(
        path, "pull {update}".format(update="-u" if update else ""))

    assert "repository default not found" not in status, \
        "Can't pull repo '{path}'".format(path=path)
    return status


@logged(timed=True, print_args=True)
def try_repo_rebase(path):
    """
    $ hg heads default
    changeset:   2:404380be8e16
    tag:         tip
    parent:      0:b14ca0776671
    user:        v.taranov <v.taranov@nexters.com>
    date:        Mon Oct 17 16:28:51 2016 +0300
    summary:     3

    changeset:   1:e6ef9c8a1684
    user:        v.taranov <v.taranov@nexters.com>
    date:        Mon Oct 17 16:28:16 2016 +0300
    summary:     2
    """
    branch = _repo_cmd(path, 'branch').strip()
    heads = _repo_cmd(path, 'heads {branch}'.format(branch=branch))
    if heads.count('changeset:') > 1:
        log.warn("try rebasing")
        repo_rebase(path)


@logged(timed=True, print_args=True)
def repo_rebase(path):
    status = _repo_cmd(path, "rebase")
    # if "nothing to rebase" in status:
    #     raise RepoError("Nothing to rebase")
    return status


@logged(print_args=True, timed=True)
def repo_uncommitted_changes(path):
    return _repo_cmd(path, "status")


@logged(print_args=True, timed=True)
def repo_commit(path, message, addremove=True):
    if not repo_uncommitted_changes(path):
        return NOTHING_TO_COMMIT_MSG

    if addremove:
        cmd = "addremove"
        _repo_cmd(path, cmd)

    cmd = "commit -m '{message}'".format(message=message)
    return _repo_cmd(path, cmd)


@logged(timed=True)
def repo_push(path):
    cmd = "push"

    return _repo_cmd(path, cmd)


@logged(timed=True)
def repo_update(path, branch, clean=True):
    cmd = "update {branch}".format(branch=branch)
    if clean:
        cmd += ' -C'
    return _repo_cmd(path, cmd)


@logged(timed=True)
def repo_update_to_revision(path, revision, clean=True):
    cmd = "update -r {revision}".format(revision=revision)
    if clean:
        cmd += ' -C'
    return _repo_cmd(path, cmd)


def repo_revision(path):
    revision = _repo_cmd(path, "identify --num -R")
    if revision:
        if "+" in revision:
            revision = revision.split('+', 1)[0]
        return int(revision)

    return 0


def repo_current_revision_hash(path):
    h = _repo_cmd(path, "log --template '{node}\r\n' -r .")
    return h.strip()


def repo_has_incoming_commits(path):
    """
    branch: default
    commit: (clean)
    update: (current)
    phases: 1 draft
    remote: 1 outgoing


    [atlas_builder] auto rebase
    branch: default
    commit: 1 modified
    update: (current)
    remote: (synced)


    branch: exiftool
    commit: (head closed)
    update: (current)
    phases: 30 draft
    remote: 1 or more incoming

    """
    summary = _repo_cmd(path, "summary --remote")
    remote = summary.split('remote:')[-1]
    return 'incoming' in remote


def repo_branch(path, branch):
    if not branch:
        raise RepoError('Empty branch')

    return _repo_cmd(path, "branch " + branch)


def repo_branches(path):
    return _repo_cmd(path, "branches")


if __name__ == '__main__':
    try_repo_rebase('/Users/vtaranov/Desktop/tmp')
