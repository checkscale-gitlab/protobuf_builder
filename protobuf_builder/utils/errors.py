class BaseError(Exception):
    pass


class BaseOSError(BaseError):
    pass


######################################################


class BadCommandError(BaseError):
    pass


class FileNotFoundError(BaseOSError):
    pass


class AssetsManagerError(BaseError):
    pass


class AbortedError(BaseError):
    pass


class PrebuildError(BaseError):
    pass


######################################################
class BadGameVersionError(AssetsManagerError):
    pass


class AssetsNotSyncedError(AssetsManagerError):
    pass


class BadAssetsVersionError(AssetsManagerError):
    pass


class BadBundleFileError(AssetsManagerError):
    pass


class DuplicateAssetError(AssetsManagerError):
    pass


######################################################
class BaseSyncronizeError(BaseError):
    pass


class RunningLocked(BaseSyncronizeError):
    pass


class ValidationError(BaseError):
    pass
