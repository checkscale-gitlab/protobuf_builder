# -*- coding: utf-8 -*-
from .utils.cli import create_parser, parse_args
from . import __version__


def build_protocol():
    pass


if __name__ == '__main__':
    parser = create_parser(version=__version__)
    args = parse_args(__file__, parser)
    build_protocol()
